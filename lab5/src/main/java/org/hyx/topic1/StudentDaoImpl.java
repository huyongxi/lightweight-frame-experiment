package org.hyx.topic1;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * StudentDao的实现类
 */

public class StudentDaoImpl implements StudentDao {
    //定义JdbcTemplate属性及其getter和setter方法
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 添加学生
     */
    public int addStudent(Student stu) {

        //定义SQL语句
        String sql = "insert into users(id,num,userName,password,sex,regDate) values (?,?,?,?,?,?)";
        //存放SQL语句的参数
        Object[] params = new Object[]{
                stu.getId(),
                stu.getNum(),
                stu.getUserName(),
                stu.getPassword(),
                stu.getSex(),
                stu.getRegDate()
        };
        //执行SQL，获取返回结果
        int flag = jdbcTemplate.update(sql, params);
        return flag;
    }

    /**
     * 更新学生数据
     */
    public int updateStudent(Student stu) {
        //定义SQL语句
        String sql = "update users set num = ?,userName=?,password=?,sex=?,regDate=? where id = ?";
        //存放SQL语句的参数
        Object[] params = new Object[]{
                stu.getNum(),
                stu.getUserName(),
                stu.getPassword(),
                stu.getSex(),
                stu.getRegDate(),
                stu.getId()
        };
        //执行SQL，获取返回结果
        return jdbcTemplate.update(sql, params);
    }

    /**
     * 根据学号num删除学生
     */
    public int deleteStudentByNum(String num) {
        //定义SQL语句
        String sql = "delete from users where num = ?";
        //执行SQL，获取返回结果
        int flag = jdbcTemplate.update(sql, num);
        return flag;
    }

    /**
     * 根据id查询学生
     */
    public Student findStudentByNum(String num) {
        //定义SQL语句
        String sql = "select * from users where num = ?";
        /*
         * 提供默认实现类 BeanPropertyRowMapper，使用此类要求数据表的列必须和java对象的属性对应
         *  BeanPropertyRowMapper将结果集通过java的反射机制映射到java对象中
         */
        RowMapper<Student> rowMapper = BeanPropertyRowMapper
                .newInstance(Student.class);
        //使用queryForObject方法查询，返回单行记录
        Student student = jdbcTemplate.queryForObject(sql, rowMapper, num);
        return student;
    }

    /**
     * 查询所有学生
     */
    public List<Student> findAllStudents() {
        //定义SQL语句
        String sql = "select * from users";
		/*
		 * 提供默认实现类 BeanPropertyRowMapper ， javabean属性和表的字段必须一致
		   BeanPropertyRowMapper将结果集通过java的反射机制映射到java对象中
		 */
        RowMapper<Student> rowMapper = BeanPropertyRowMapper.newInstance(Student.class);
        //使用query方法执行查询，并返回一个集合
        return jdbcTemplate.query(sql, rowMapper);
    }
}
