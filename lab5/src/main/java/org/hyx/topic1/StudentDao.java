package org.hyx.topic1;
import java.util.List;
public interface StudentDao {
	//添加学生
	public int addStudent(Student stu);
	//更新学生
	public int updateStudent(Student stu);
	//根据学号num删除学生
	public int deleteStudentByNum(String num);
	//根据学号num查询
	public Student findStudentByNum(String num);
	//查询所有学生
	public List<Student> findAllStudents();
}
