package org.hyx.topic1;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class CreateTableTest {
    public static void main(String[] args) {
        //1 spring 内置数据源
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        // * 基本4项
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/ssmbuild?useUnicode=true&characterEncoding=UTF-8" +
                "&serverTimezone=Asia/Shanghai");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        // 2 jdbc模板开发，必须提供数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "CREATE TABLE  If Not Exists users("
                + " id int(10) NOT NULL auto_increment, "
                + "num varchar(15) NOT NULL,"
                + " userName varchar(50) NOT NULL,"
                + " password varchar(50) NOT NULL,"
                + " sex varchar(10) NOT NULL,"
                + " regDate varchar(20) DEFAULT NULL,"
                + " PRIMARY KEY (id,num)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        // 3 调用jdbc模板的exexute()方法创建users数据表
        jdbcTemplate.execute(sql);

        System.out.println("创建表成功！");
    }
}
