package org.hyx.topic2;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 实体类Student
 */
@Data
@Accessors(chain = true)
@ToString
public class Student {
	private int id;
	private String num; // 学号
	private String userName; // 姓名
	private String password; // 密码
	private String sex; // 性别
	private String regDate; // 注册日期
}
