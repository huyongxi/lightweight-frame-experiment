package org.hyx.dao;

import org.hyx.pojo.User;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	public boolean loginUser(String stuno,String stupwd){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";
			String user="root";
			String pwd="root";
			Connection conn=DriverManager.getConnection(url,user,pwd);
			PreparedStatement ps = null;
			ps=conn.prepareStatement("select * from t_student where stuno=? and stupwd=?");
			ps.setString(1, stuno);
			ps.setString(2, stupwd);
			ResultSet rs=ps.executeQuery();
			if (rs.next())
				return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean registerUser(User u){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";
			String user="root";
			String pwd="root";
			Connection conn=DriverManager.getConnection(url,user,pwd);
			PreparedStatement ps = null;
			ps=conn.prepareStatement("insert into t_student values (?,?,?,?)");
			ps.setString(1, u.getStuno());
			ps.setString(2, u.getStupwd());
			ps.setString(3, u.getStuname());
			ps.setString(4, u.getStusex());
			return ps.execute();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}



}
