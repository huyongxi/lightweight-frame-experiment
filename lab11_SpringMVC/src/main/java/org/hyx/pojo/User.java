package org.hyx.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author hyx
 * @Date 2021/11/16 19:19
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class User {
    private String stuno;
    private String stupwd;
    private String stuname;
    private String stusex;
}
