package org.hyx.controller;

import org.hyx.dao.UserDao;
import org.hyx.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginController extends AbstractController {
    @Autowired
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping("login.do")
    public ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        if (userDao.loginUser(req.getParameter("userName"), req.getParameter("password")))
            return new ModelAndView("succ.jsp");
        else
            return new ModelAndView("/index.jsp");
    }




}
