package org.hyx.controller;

import org.hyx.dao.UserDao;
import org.hyx.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author hyx
 * @Date 2021/11/16 19:38
 * @Version 1.0
 */
@Controller
public class RegisterController {
    @Autowired
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    @RequestMapping("register.do")
    protected ModelAndView registerHandler(HttpServletRequest req, HttpServletResponse resp, User user) throws Exception {
        boolean b = userDao.registerUser(user);
        return new ModelAndView("/index.jsp");
    }
}
