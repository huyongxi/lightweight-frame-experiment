package org.hyx.dao.bill;

import org.apache.ibatis.annotations.Mapper;
import org.hyx.pojo.Bill;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface BillMapper {
	/**
	 * 查询订单列表
	 * @param bill
	 * @return
	 */
	public List<Bill> getBillList(Bill bill);
	
}
