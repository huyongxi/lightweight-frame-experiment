package org.hyx.dao.provider;

import org.hyx.pojo.Provider;

import java.util.List;


public interface ProviderMapper {
    /**
     * 查询供应商表记录数
     *
     * @return
     */
    int count();

    /**
     * 查询供应商列表
     *
     * @return
     */
    List<Provider> getProviderList();

    /**
     * 根据供应商名称查询供应商列表
     *
     * @return
     */
    List<Provider> getProviderListByName(String proName);

}
