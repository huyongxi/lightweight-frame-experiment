package org.hyx.service.provider;

import java.util.List;

import org.hyx.dao.provider.ProviderMapper;
import org.hyx.pojo.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("providerService")
public class ProviderServiceImpl implements ProviderService {

    @Autowired
	private ProviderMapper providerMapper;

    @Override
    public int getCountOfProviders() {
        try {
            return providerMapper.count();
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<Provider> findAllProviders() {
        try {
            return providerMapper.getProviderList();
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }


    @Override
    public List<Provider> findProvidersByName(String proName) {
        try {
            return providerMapper.getProviderListByName(proName);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public ProviderMapper getProviderMapper() {
        return providerMapper;
    }

    public void setProviderMapper(ProviderMapper providerMapper) {
        this.providerMapper = providerMapper;
    }

}
