package org.hyx.service.provider;

import org.hyx.pojo.Provider;

import java.util.List;

public interface ProviderService {
    public int getCountOfProviders();
    public List<Provider> findAllProviders();
    public List<Provider> findProvidersByName(String proName);
}
