package org.hyx.service.bill;

import java.util.List;

import org.hyx.dao.bill.BillMapper;
import org.hyx.pojo.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("billService")
public class BillServiceImpl implements BillService {
    @Autowired
	private BillMapper billMapper;
	
    @Override
    public List<Bill> findBillByConditions(Bill bill) {
        try {
            return billMapper.getBillList(bill);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public BillMapper getBillMapper() {
        return billMapper;
    }

    public void setBillMapper(BillMapper billMapper) {
        this.billMapper = billMapper;
    }

}
