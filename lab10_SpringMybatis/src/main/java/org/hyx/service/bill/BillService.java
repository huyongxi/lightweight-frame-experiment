package org.hyx.service.bill;

import org.hyx.pojo.Bill;

import java.util.List;


public interface BillService {
    List<Bill> findBillByConditions(Bill bill);
}
