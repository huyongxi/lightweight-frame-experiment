package org.hyx.bill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hyx.pojo.Bill;
import org.hyx.service.bill.BillService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class BillTest {
    private Logger logger = Logger.getLogger(BillTest.class);


    @Test
    public void test(){

    }





    @Test
    public void testGetBillList() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "bean1.xml");
        BillService billService = (BillService) ctx.getBean("billService");
        List<Bill> billList = new ArrayList<Bill>();
        Bill bill = new Bill();
        bill.setIsPayment(2);
        bill.setProductName("油");
        bill.setProviderId(7);
        billList = billService.findBillByConditions(bill);
       System.out.println("--"+billList.size());
        for (Bill element : billList) {
            logger.debug("testGetBillList id: "
                    + element.getId()
                    + " and BillCode: "
                    + element.getBillCode()
                    + " and ProductName: "
                    + element.getProductName()
                    + " and ProviderName: "
                    + element.getProviderName()
                    + " and TotalPrice: "
                    + element.getTotalPrice()
                    + " and IsPayment: "
                    + element.getIsPayment()
                    + " and CreationDate:"
                    + new SimpleDateFormat("yyyy-MM-dd").format(element
                            .getCreationDate()));
        }

    }
}
