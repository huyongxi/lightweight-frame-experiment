package org.hyx.provider;

import org.apache.log4j.Logger;
import org.hyx.pojo.Provider;
import org.hyx.service.provider.ProviderService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;


public class ProviderTest {

    private Logger logger = Logger.getLogger(ProviderTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGetProviderList() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "bean2.xml");
        ProviderService providerService = (ProviderService) ctx
                .getBean("providerService");
        List<Provider> providerList = new ArrayList<Provider>();
        providerList = providerService.findProvidersByName("厂");

        for (Provider provider : providerList) {
            logger.debug("testGetProviderList proCode: "
                    + provider.getProCode() + " and proName: "
                    + provider.getProName());
        }
    }

}
