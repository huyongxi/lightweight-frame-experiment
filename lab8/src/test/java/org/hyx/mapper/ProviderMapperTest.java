package org.hyx.mapper;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.hyx.pojo.Provider;
import org.hyx.utils.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class ProviderMapperTest {
    private Logger logger = Logger.getLogger(ProviderMapperTest.class);

    SqlSession session;
    ProviderMapper1 mapper;

    @Before
    public void setUp() throws Exception {
        session = MyBatisUtil.getSession();
        mapper = session.getMapper(ProviderMapper1.class);
    }

    @Test
    public void testGetProviderByLikeCodeOrName() {
        mapper.getProviderByLikeCodeOrName("001", "公司").forEach(System.out::println);
    }

    @Test
    public void testUpdateProvider() {
		int update_id=13;
        System.out.println("更新之前===>"+mapper.selectOne(update_id).toString());
        Provider provider = new Provider(update_id, "ST_JB3026", "成都BIGM有限公司", "长期合作伙伴,主营:啥都有", "路飞", "10086111666",
                "汕头市潮阳区"
                , "1a231dsa1sd153", 1,
                null, new Date(), 2);
        mapper.updateProvider(provider);
        System.out.println("更新之前===>"+mapper.selectOne(update_id).toString());
    }


    @Test
    public void getRecordsByChoose(){
        List<Provider> list =
                mapper.getRecordsByChoose(new Provider().setProPhone("BJ").setProName("公司").setProContact("王").setCreationDate(new Date()));
        list.forEach(System.out::println);
    }

    @Test
    public void getRecordByList(){
        long count = mapper.getRecords().stream().count();
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex",0);
        map.put("pageSize",(int)(count/2));
        mapper.getRecordByLimit(map).forEach(System.out::println);
    }


    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }



}