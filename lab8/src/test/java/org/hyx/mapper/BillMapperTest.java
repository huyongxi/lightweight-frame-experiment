package org.hyx.mapper;

import org.apache.ibatis.session.SqlSession;
import org.hyx.utils.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BillMapperTest {

    SqlSession sqlSession;
    BillMapper mapper;

    @Before
    public void setUp() {
        sqlSession = MyBatisUtil.getSession();
        mapper = sqlSession.getMapper(BillMapper.class);
    }

    @Test
    public void getBillRecordsByProIds() {
        Integer[] ids =
                sqlSession.getMapper(ProviderMapper1.class).getRecords().stream().map(record -> record.getId()).toArray(Integer[]::new);
        mapper.getBillRecordsByProIds(ids).forEach(System.out::println);
    }

    @Test
    public void getBillRecordsByProIdsOnList() {

        List<Integer> collect =
                sqlSession.getMapper(ProviderMapper1.class).getRecords().stream().map(record -> record.getId()).collect(Collectors.toList());
        mapper.getBillRecordsByProIdsOnList(collect).forEach(System.out::println);
    }

    @Test
    public void getBillRecordsByBillCodeProviders(){
        List<Integer> ids =
                sqlSession.getMapper(ProviderMapper1.class).getRecords().stream().map(record -> record.getId()).collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("map_billCode","LL2016_01");
        map.put("ids",ids);
        mapper.getBillRecordsByBillCodeAndProviders(map).forEach(System.out::println);
    }



    @After
    public void tearDown() {
        sqlSession.commit();
        sqlSession.close();
    }


}
