package org.hyx.mapper;

import org.apache.ibatis.annotations.Param;
import org.hyx.pojo.Bill;

import java.util.List;
import java.util.Map;

public interface BillMapper {
    List<Bill> getRecords();

    /**
     * 分页
     *
     * @param map
     * @return
     */
    List<Bill> getRecordByLimit(@Param("map") Map<String, Integer> map);

    /**
     * 根据订单编码和指定供应商查询订单列表
     *
     * @param map
     * @return
     */
    List<Bill> getBillRecordsByBillCodeAndProviders(@Param("map") Map<String, Object> map);

    /**
     * 根据供应商id查询订单列表
     */
    List<Bill> getBillRecordsByProIds(@Param("ids") Integer ids[]);

    /**
     * 根据供应商id查询订单列表
     * 入参类型为list
     *
     * @param ids
     * @return
     */
    List<Bill> getBillRecordsByProIdsOnList(@Param("ids") List<Integer> ids);


}
