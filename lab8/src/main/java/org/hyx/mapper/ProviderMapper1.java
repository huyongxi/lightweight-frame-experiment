package org.hyx.mapper;

import org.apache.ibatis.annotations.Param;
import org.hyx.pojo.Provider;

import java.util.List;
import java.util.Map;

public interface ProviderMapper1 {

    //分页
    List<Provider> getRecordByLimit(@Param("map") Map<String, Integer> map);

    //根据供应商编码或者名称进行模糊查询
    List<Provider> getRecordsLikeCodeOrName(@Param("proCode") String proCode, @Param("proName") String proName);

    //choose查询供应商列表
    List<Provider> getRecordsByChoose(Provider provider);

    //查询供应商列表
    List<Provider> getRecords();

    //根据供应商编号或名称获取供应商信息
    List<Provider> getProviderByLikeCodeOrName(@Param("proCode") String proCode, @Param("proName") String proName);

    //根据id查找供应商
    Provider selectOne(Integer id);

    //更新供应商信息
    void updateProvider(Provider provider);


}
