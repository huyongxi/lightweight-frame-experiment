package com.hyx.Factory;


import com.hyx.Pojo.Student;

public class Factory2 {
    /**
     * 实例工厂创建Student
     */
    public Student getStudent(){
        return new Student();
    }
}
