package com.hyx.Factory;


import com.hyx.Pojo.Student;

public class Factory1 {
    /**
     * 静态工厂创建Student
     */
    public static Student getStudent(){
        return new Student();
    }
}
