package com.hyx.Pojo;

public class Student {
    private String sNo;
    private String sName;
    private int age;

    public Student() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Student(String sNo, String sName, int age) {
        super();
        this.sNo = sNo;
        this.sName = sName;
        this.age = age;
    }


    public void setsNo(String sNo) {
        this.sNo = sNo;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return " [sNo=" + sNo + ", sName=" + sName + ", age=" + age + "]";
    }


}
