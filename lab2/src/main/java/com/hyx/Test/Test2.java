package com.hyx.Test;

import com.hyx.Pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2 {
    public static void main(String[] args) {
        // 以类加载路径下的beans.xml文件创建Spring容器
        ApplicationContext context=new ClassPathXmlApplicationContext("application.xml");
        // 判断两次请求singleton作用域的Bean实例是否相等
        System.out.println("singleton作用域:");
        User userA1 = context.getBean("user1", User.class);
        User userA2 = context.getBean("user1", User.class);
        System.out.println("userA1 = " + userA1);
        System.out.println("userA2 = " + userA2);
        System.out.println("userA1==userA2"+(userA1==userA2));

        System.out.println("-------------------------------");
        System.out.println("prototype作用域:");
        // 判断两次请求prototype作用域的Bean实例是否相等
        User userB1 = context.getBean("user2", User.class);
        User userB2 = context.getBean("user2", User.class);
        System.out.println("userB1 = " + userB1);
        System.out.println("userB2 = " + userB2);
        System.out.println("userB1==userB2"+(userB1==userB2));
    }
}
