package com.hyx.Test;


import com.hyx.Pojo.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test1 {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        //构造器创建
        Student student1 = context.getBean("student1", Student.class);
        //静态工厂
        Student student2 = context.getBean("student2", Student.class);
        //实例化工厂
        Student student3 = context.getBean("student3", Student.class);

        System.out.println("student1 = " + student1);
        System.out.println("student2 = " + student2);
        System.out.println("student3 = " + student3);

    }

}
