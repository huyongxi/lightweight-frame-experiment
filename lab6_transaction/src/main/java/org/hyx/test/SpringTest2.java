package org.hyx.test;

import org.hyx.pojo.User;
import org.hyx.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class SpringTest2 {

    static ApplicationContext context;
    static UserServiceImpl userService;

    @BeforeAll
    public static void setUp() {
        context = new ClassPathXmlApplicationContext("beans-annotation.xml");
        userService = context.getBean("userService", UserServiceImpl.class);

    }

    @Test
    public void testAdd() {
        System.out.printf("添加记录数：%d",
                userService.addUser(new User().setUsername(new Date().toString()).setPassword("123456")));
        System.out.printf("删除记录数：%d", userService.deleteUserById(5));
    }

    @Test
    public void tesDel() {
        System.out.printf("删除记录数：%d", userService.deleteUserById(4));
    }

    @Test
    public void testUpdate() {
        System.out.printf("更新记录数：%d",
                userService.updateUser(new User().setUserid(123).setUsername("晴天").setPassword("123456")));
    }

    @Test
    public void testFind() {
        List<String> collect = userService.findAllUser().stream().map(User::getUsername).collect(Collectors.toList());
        collect.forEach(System.out::println);
    }
}