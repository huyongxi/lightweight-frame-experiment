package org.hyx.service.impl;

import org.hyx.dao.UserDao;
import org.hyx.pojo.User;
import org.hyx.service.UserService;

import java.util.List;


public class UserServiceImpl implements UserService {

    private UserDao userdao;

    public UserDao getUserdao() {
        return userdao;
    }

    public void setUserdao(UserDao userdao) {
        this.userdao = userdao;
    }

    @Override
    public int addUser(User user) {
        // TODO Auto-generated method stub
        int n = this.userdao.addUser(user);
        return n;
    }

    @Override
    public int updateUser(User user) {
        // TODO Auto-generated method stub
        int n = this.userdao.updateUser(user);
        return n;
    }

    @Override
    public int deleteUserById(int id) {
        // TODO Auto-generated method stub
        int n = this.userdao.deleteUserById(id);
        return 0;
    }

    @Override
    public User findUserById(int id) {
        // TODO Auto-generated method stub
        User user = this.userdao.findUserById(id);
        return null;
    }

    @Override
    public List<User> findAllUser() {
        // TODO Auto-generated method stub
        List<User> list = this.userdao.findAllUser();
        return null;
    }

}
