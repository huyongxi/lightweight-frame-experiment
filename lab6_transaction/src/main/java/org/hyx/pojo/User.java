package org.hyx.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 实体类User
 */
@Data
@Accessors(chain = true)
public class User {
	private Integer userid; //用户id
	private String username; //用户名
	private String password; //密码

	@Override
	public String toString() {
		return "UserDao [userid=" + userid + ", username=" + username
				+ ", password=" + password + "]";
	}
}
