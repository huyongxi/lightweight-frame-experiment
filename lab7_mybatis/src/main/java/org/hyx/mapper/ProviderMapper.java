package org.hyx.mapper;

import org.apache.ibatis.annotations.Param;
import org.hyx.pojo.Provider;

import java.util.List;

public interface ProviderMapper {

    /*
     * 查询供应商总记录数
     */
    int findAllProvidersNum();

    /*
     * 根据id查找供应商
     */
    Provider selectOne(Integer id);

    /*
     * 查询供应商列表
     */
    List<Provider> findAllProvider();

    /*
     * 添加供应商表
     */
    void addProvider(Provider provider);

    /*
     * 修改供应商表
     */
    void updateProvider(Provider provider);

    /*
     * 删除供应商表的记录
     */
    void deleteProvider(int id);

}
