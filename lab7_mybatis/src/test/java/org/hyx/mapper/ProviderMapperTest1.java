package org.hyx.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class ProviderMapperTest1 {
	private Logger logger = Logger.getLogger(ProviderMapperTest1.class);
	
	@Before
	public void setUp() throws Exception {
	}





	@Test
	public void test() {
		String resource = "mybatis-config.xml";
		int count = 0;
		SqlSession sqlSession = null;
		try {
			//1 获取mybatis-config.xml的输入流
			InputStream inputStream = Resources.getResourceAsStream(resource);
			//2 创建SqlSessionFactory对象，完成对配置文件的读取
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			//3 创建sqlSession
			sqlSession=sqlSessionFactory.openSession();
			//4 调用mapper文件来对数据进行操作，必须先把mapper文件引入到mybatis-config.xml中
			ProviderMapper mapper = sqlSession.getMapper(ProviderMapper.class);

			logger.info("查询供应商表的总记录数"+mapper.findAllProvidersNum());

		} catch (IOException e) {
			e.printStackTrace();
		} finally{
           //5 关闭sqlSession
			sqlSession.close();
		}
	}

}

