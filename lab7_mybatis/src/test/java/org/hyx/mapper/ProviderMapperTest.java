package org.hyx.mapper;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.hyx.pojo.Provider;
import org.hyx.utils.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ProviderMapperTest {
    private Logger logger = Logger.getLogger(ProviderMapperTest.class);

    SqlSession session;
    ProviderMapper mapper;

    @Before
    public void setUp() throws Exception {
        session = MyBatisUtil.getSession();
        mapper = session.getMapper(ProviderMapper.class);
    }


    @Test
    public void testCount() throws Exception {
        //创建SqlSession的会话对象
        SqlSession sqlSession = MyBatisUtil.getSession();
        // SqlSession执行映射文件中定义的SQL，并返回映射结果
        //第一种方式:调用selectOne方法执行查询操作
        Provider provider1 = sqlSession.selectOne("org.hyx.mapper.ProviderMapper.selectOne", 1);
        //第二种方式:调用getMapper(Mapper.class)执行dao接口方法来实现对数据库的查询操作
        ProviderMapper mapper = sqlSession.getMapper(ProviderMapper.class);
        Provider provider2 = mapper.selectOne(1);
        // 打印输出结果
        System.out.println("provider1 = " + provider1);
        System.out.println("provider2 = " + provider2);
        // 关闭SqlSession
        sqlSession.close();
    }

    @Test
    public void testGetProviderList() throws Exception {
        //创建ArrayList列表对象，用来保存查询的供应商列表
        List<Provider> providers1 = new ArrayList<>();
        //创建SqlSession的会话对象
        SqlSession sqlSession = MyBatisUtil.getSession();
        // SqlSession执行映射文件中定义的SQL，并返回映射结果
        //第一种方式:调用selectList方法执行查询操作
        providers1 = sqlSession.selectList("org.hyx.mapper.ProviderMapper.findAllProvider");
        //第二种方式:调用getMapper(Mapper.class)执行dao接口方法来实现对数据库的查询操作
        ProviderMapper mapper = sqlSession.getMapper(ProviderMapper.class);
        List<Provider> providers2 = mapper.findAllProvider();
        // 打印输出结果
        providers1.forEach(System.out::println);
        System.out.println("=======================================");
        providers2.forEach(System.out::println);

        // 关闭SqlSession
        sqlSession.close();
    }

    @Test
    public void testAdd() throws Exception {
        Provider provider = new Provider(null, "ST_JB3026", "广东SOUT有限公司", "长期合作伙伴,主营:啥都有", "罗罗诺亚索隆", "10086111666",
                "汕头市潮南区"
                , "1a231dsa1sd153", 1,
                new Date(), null, null);
        mapper.addProvider(provider);
    }

    @Test
    public void testUpdate() throws Exception {
        Provider provider = new Provider(18, "ST_JB3026", "成都BIGM有限公司", "长期合作伙伴,主营:啥都有", "路飞", "10086111666",
                "汕头市潮阳区"
                , "1a231dsa1sd153", 1,
                null, new Date(), 2);
        mapper.updateProvider(provider);
    }

    @Test
    public void testDelete() throws Exception {
        mapper.deleteProvider(18);
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }


}