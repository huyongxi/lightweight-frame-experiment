package org.hyx.topic1;

import org.hyx.topic1.action.PersonAction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test {

	public static void main(String[] args){
		//Spring容器初始化，加载Spring配置文件
		ApplicationContext context = new ClassPathXmlApplicationContext("beans1.xml");
		PersonAction userAction=(PersonAction) context.getBean("personAction");
		String result=userAction.execute();
	}

}
