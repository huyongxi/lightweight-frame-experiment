package org.hyx.topic1.aop;

public class AuthAspect {
    public void validateUser() {
        System.out.println("验证用户");
    }

    public void before() {
        System.out.println("业务处理之前！");
    }

    public void after() {
        System.out.println("业务处理之后！");
    }

}
