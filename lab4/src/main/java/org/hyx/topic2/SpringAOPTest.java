package org.hyx.topic2;

import org.hyx.topic2.service.StudentService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SpringAOPTest {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("beans2.xml");
		StudentService student = (StudentService)context.getBean("studentService");

		student.save("1740706000", "孙南");		
		student.update("1740706001", "丁其");
		student.delete("1740706001");
	}

}
