package org.hyx.topic3;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class LogAfter implements AfterReturningAdvice {

    @Override
    public void afterReturning(Object o, Method method, Object[] objects, Object o1) throws Throwable {
        System.out.printf("【%s】:  %s%s() 方法被调用！\n",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDateTime.now()), o1.getClass().getName(),
                method.getName());
    }
}