package org.hyx.topic3;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class LogAround implements MethodInterceptor {

    public Object invoke(MethodInvocation invocation) throws Throwable {
        String name = invocation.getMethod().getName();
        System.out.printf("开始执行[%s]方法!\n",name);
        Object proceed = invocation.proceed();
        System.out.printf("结束执行[%s]方法!\n",name);
        return proceed;
    }
}