package org.hyx.mapper;

import org.apache.ibatis.session.SqlSession;
import org.hyx.pojo.Bill;
import org.hyx.utils.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.hyx.mapper.BillMapper;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author hyx
 * @Date 2021/11/8 0:38
 * @Version 1.0
 */
public class BillMapperTest {

    SqlSession sqlSession;
    BillMapper billMapper;

    @Before
    public void setUp() throws Exception {
        sqlSession= MyBatisUtil.getSession();
        billMapper=sqlSession.getMapper(BillMapper.class);
    }

    @Test
    public void getBillList(){
        billMapper.getBillList("大米",3,2).forEach(System.out::println);
    }

    @Test
    public void getBillAssociation(){
        billMapper.getBillAssociation("大米",3,2).forEach(System.out::println);
    }


    @After
    public void tearDown() throws Exception {
        sqlSession.commit();
        sqlSession.close();
    }
}