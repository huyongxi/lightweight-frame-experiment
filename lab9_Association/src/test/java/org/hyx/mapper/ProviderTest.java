package org.hyx.mapper;

import org.apache.ibatis.session.SqlSession;
import org.hyx.pojo.Provider;
import org.hyx.utils.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author hyx
 * @Date 2021/11/8 10:44
 * @Version 1.0
 */
public class ProviderTest {
    SqlSession session;
    ProviderMapper mapper;

    @Before
    public void setUp() throws Exception {
        session= MyBatisUtil.getSession();
        mapper=session.getMapper(ProviderMapper.class);
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }

    @Test
    public void getProviderListOneToMany() {
        mapper.getProviderListOneToMany().forEach(System.out::println);
    }
}