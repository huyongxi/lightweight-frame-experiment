package org.hyx.mapper;

import org.hyx.pojo.Provider;

import java.util.List;

public interface ProviderMapper {

    List<Provider> getProviderListOneToMany();

    /**
    * deleteByPrimaryKey
    * @param id id
    * @return int int
    */
    int deleteByPrimaryKey(Long id);

    /**
    * insert
    * @param record record
    * @return int int
    */
    int insert(Provider record);

    /**
    * insertSelective
    * @param record record
    * @return int int
    */
    int insertSelective(Provider record);

    /**
    * selectByPrimaryKey
    * @param id id
    * @return Provider Provider
    */
    Provider selectByPrimaryKey(Long id);

    /**
    * updateByPrimaryKeySelective
    * @param record record
    * @return int int
    */
    int updateByPrimaryKeySelective(Provider record);

    /**
    * updateByPrimaryKey
    * @param record record
    * @return int int
    */
    int updateByPrimaryKey(Provider record);
}