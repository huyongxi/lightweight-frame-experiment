package org.hyx.mapper;

import org.apache.ibatis.annotations.Param;
import org.hyx.pojo.Bill;

import java.util.List;

public interface BillMapper {


    List<Bill> getBillAssociation(@Param("proName")String proName,@Param("providerId")Integer providerId,
                                  @Param("ispayment")Integer ispayment);


    List<Bill> getBillList(@Param("proName")String proName,@Param("providerId")Integer providerId,
                           @Param("ispayment")Integer ispayment);





    /**
    * deleteByPrimaryKey
    * @param id id
    * @return int int
    */
    int deleteByPrimaryKey(Long id);

    /**
    * insert
    * @param record record
    * @return int int
    */
    int insert(Bill record);

    /**
    * insertSelective
    * @param record record
    * @return int int
    */
    int insertSelective(Bill record);

    /**
    * selectByPrimaryKey
    * @param id id
    * @return Bill Bill
    */
    Bill selectByPrimaryKey(Long id);

    /**
    * updateByPrimaryKeySelective
    * @param record record
    * @return int int
    */
    int updateByPrimaryKeySelective(Bill record);

    /**
    * updateByPrimaryKey
    * @param record record
    * @return int int
    */
    int updateByPrimaryKey(Bill record);
}