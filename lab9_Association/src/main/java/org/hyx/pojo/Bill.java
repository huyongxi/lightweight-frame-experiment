package org.hyx.pojo;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Bill {
    /**
    * 主键ID
    */
    private Long id;

    /**
    * 账单编码
    */
    private String billcode;

    /**
    * 商品名称
    */
    private String productname;

    /**
    * 商品描述
    */
    private String productdesc;

    /**
    * 商品单位
    */
    private String productunit;

    /**
    * 商品数量
    */
    private BigDecimal productcount;

    /**
    * 商品总额
    */
    private BigDecimal totalprice;

    /**
    * 是否支付（1：未支付 2：已支付）
    */
    private Integer ispayment;

    /**
    * 创建者（userId）
    */
    private Long createdby;

    /**
    * 创建时间
    */
    private Date creationdate;

    /**
    * 更新者（userId）
    */
    private Long modifyby;

    /**
    * 更新时间
    */
    private Date modifydate;

    /**
    * 供应商ID
    */
    private Integer providerid;

    /**
     * 新增属性 供应商名
     */
    private String providerName;

    /**
     * 新增属性 供应商对象
     */
    private Provider provider;
}