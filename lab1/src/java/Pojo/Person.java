package Pojo;

import Service2.IdentityService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Data
public class Person {
    int id;
    String num;
    String userName;
    String password;
    String sex;
    String regDate;

    @Autowired
    private IdentityService service;

    public void display() throws Exception {
        int add = service.add(id, num, userName, password, sex, regDate);
        if (add > 0) {
            System.out.println("添加用户成功!!!");
            return;  //成功添加的时候就退出程序
        }
        System.out.println("添加用户失败!!!");
    }
}
