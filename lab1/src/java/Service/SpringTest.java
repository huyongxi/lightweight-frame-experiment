package Service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest{
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("application.xml");
//        IdentityService service = context.getBean("identityService", IdentityService.class);
        IdentityService service = context.getBean(IdentityService.class);
//        IdentityService service = (IdentityService) context.getBean("identityService");
        service.info("1940129123","胡永希");
    }
}
