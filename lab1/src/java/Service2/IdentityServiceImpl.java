package Service2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class IdentityServiceImpl implements IdentityService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void info(String Num, String userName) {
        System.out.println("SpringTest：num="+Num+"，userName="+userName);
    }

    @Override
    public int add(int id, String num, String userName, String password, String sex, String date) throws Exception {
        String sql="insert into users values(?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,id,num,userName,password,sex,date);
    }
}
