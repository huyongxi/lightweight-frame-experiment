package Service;

import Pojo.Person;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;


@SpringJUnitConfig(locations = "classpath:application.xml")
class ServiceTest {

    @Autowired
    @Qualifier("identityService")
    IdentityService service;

    @Autowired
    Person person;

    /**
     * 测试第一个Service
     */
    @Test
    public void test1(){
        service.info("1940***123","hyx");
    }

    /**
     * 测试第二个Service
     */
    @SneakyThrows
    @Test
    public void test2(){
        person.display();
    }


}
