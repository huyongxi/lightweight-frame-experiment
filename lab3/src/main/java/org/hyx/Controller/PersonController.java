package org.hyx.Controller;


import org.hyx.Service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class PersonController {
    @Autowired
    @Qualifier("personService2")
    private PersonService personService;

    public void setUserService(PersonService personService) {
        this.personService = personService;
    }

    public PersonService getPersonService() {
        return personService;
    }

    public int countPet() {
        System.out.println("personController...countPet...");
        return this.personService.countPet();
    }
}
