package org.hyx.Service.impl;


import org.hyx.Dao.PersonDao;
import org.hyx.Service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("personService")
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonDao personDao;

    public PersonDao getPersonDao() {
        return personDao;
    }

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    //计算用户有几个宠物
    @Override
    public int countPet() {
        System.out.println("CountPet in PersonServiceImpl1");
        return this.personDao.countPet();
    }

    //喂养宠物
    @Override
    public void feed() {
        System.out.println("Feed in PersonServiceImpl1");
        this.personDao.feed();
    }

}
