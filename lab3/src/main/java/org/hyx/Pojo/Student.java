package org.hyx.Pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
@ToString
public class Student {
    private String sNo;
    private String sName;
    private String[] languages;
    private List<String> favors;
    private Set s;
    private Map<String, Float> scores;
    private String birth;

    public Student(String sNo, String sName, String birth) {
        this.sNo = sNo;
        this.sName = sName;
        this.birth = birth;
    }

}
