package org.hyx.Pojo;

import org.springframework.stereotype.Component;

@Component
public class Person {
	private Cat cat;
    private Dog dog;
    private String pName;//姓名

    public Person() {
    }

    public Person(Cat cat, Dog dog) {
        this.cat = cat;
        this.dog = dog;
    }

    public Cat getCat() {
        return cat;
    }
    public void setCat(Cat cat) {
        this.cat = cat;
    }
    public Dog getDog() {
        return dog;
    }
    public void setDog(Dog dog) {
        this.dog = dog;
    }
    public String getPName() {
        return pName;
    }
    public void setPName(String str) {
        this.pName = str;
    }
}
