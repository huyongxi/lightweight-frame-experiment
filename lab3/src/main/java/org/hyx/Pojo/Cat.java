package org.hyx.Pojo;

import org.springframework.stereotype.Component;

@Component
public class Cat extends Animal {

    @Override
	public void feed() {
        System.out.println("Feed fish~");
    }

    @Override
	public void shout() {
        System.out.println("miao~");
    }
}
