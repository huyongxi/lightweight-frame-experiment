package org.hyx.Pojo;

import org.springframework.stereotype.Component;

@Component
public class Dog extends Animal {

    @Override
    public void feed() {
        System.out.println("Feed bone~");
    }

    @Override
    public void shout() {
        System.out.println("wang~");
    }
}
