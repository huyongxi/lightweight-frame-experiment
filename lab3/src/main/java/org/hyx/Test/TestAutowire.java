package org.hyx.Test;

import org.hyx.Pojo.Cat;
import org.hyx.Pojo.Dog;
import org.hyx.Pojo.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestAutowire {
    public static void main(String[] args) {
//加载applicationContext.xml
		ApplicationContext context = new ClassPathXmlApplicationContext("bean1-autowire.xml");
		//获取实例
		Person person1 = (Person) context.getBean("person1");
		Person person2 = (Person) context.getBean("person2");
		Person person3 = (Person) context.getBean("person3");
		printMessage(person1);
		printMessage(person2);
		printMessage(person3);

    }

    private static void printMessage(Person person) {
    	//获取person的dog
		Dog dog = person.getDog();
		//获取person的cat
		Cat cat = person.getCat();

		//调用方法
        System.out.println("人的信息:");
        System.out.println("\t称呼:" + person.getPName());
        System.out.println("Dog信息:");
		System.out.println("\t姓名:"+dog.getName());
		System.out.println("\t年龄:"+dog.getAge());
		System.out.print("\t喂养:");
		dog.feed();
		System.out.print("\t叫声:");
		dog.shout();
		System.out.println("Cat信息:");
		System.out.println("\t姓名:"+cat.getName());
		System.out.println("\t年龄:"+cat.getAge());
		System.out.print("\t喂养:");
		cat.feed();
		System.out.print("\t叫声:");
		cat.shout();
		System.out.println("**********************************************");
    }

}

