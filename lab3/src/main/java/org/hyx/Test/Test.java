package org.hyx.Test;

import cn.hutool.core.util.ReflectUtil;
import lombok.SneakyThrows;
import org.hyx.Pojo.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Test {
    @SneakyThrows
    public static void main(String[] args) {
        //加载配置文件，实例化Spring容器
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        //测试输出实例化的学生的信息
        Student student = context.getBean("student", Student.class);

        //通过反射获取student对象的所有属性字段
        Field[] fields = ReflectUtil.getFields(student.getClass());
        //遍历输出字段名及属性值
        for (Field field : fields) {
            String name = field.getName();
            //数组转Object直接输出会变成类名+哈希码,所以需要特殊处理
            if (field.toString().contains("[]")) {
                System.out.println(name + ":" + Arrays.toString((String[]) ReflectUtil.getFieldValue(student,
                        name)));
                continue;
            }
            System.out.println(name + ":" + ReflectUtil.getFieldValue(student, name));
        }
    }
}
