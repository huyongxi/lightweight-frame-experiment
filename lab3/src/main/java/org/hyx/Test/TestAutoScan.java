package org.hyx.Test;


import org.hyx.Controller.PersonController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAutoScan {
    public static void main(String[] args) {
        //加载applicationContext.xml
        ApplicationContext context = new ClassPathXmlApplicationContext("bean2-autoScan.xml");
        // 获取PersonController实例
        PersonController personController = context.getBean(PersonController.class);
        //调用PersonController中的countPet()方法，输出宠物数量
        System.out.println(personController.countPet());
        //调用PersonController中的feed()方法，喂养宠物
		personController.getPersonService().feed();

    }
}
