package org.hyx.Dao.impl;


import org.hyx.Dao.PersonDao;
import org.springframework.stereotype.Repository;

@Repository
public class PersonDaoImpl implements PersonDao {
	@Override
	public int countPet(){
		System.out.println("personDao...countPet...");
		return 2;
	}
	@Override
	public void feed() {
		System.out.println("personDao...feed...");
	}

}
